package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class EmployeeTest {

    @Autowired
    private EmployeeRepository employeeRepository; // Assuming you have an EmployeeRepository

    @Test
    public void testSaveAndRetrieveEmployee() {
        // Create an employee
        Employee employee = new Employee("John", "Doe", "Description", null);

        // Save the employee
        employee = employeeRepository.save(employee);

        // Retrieve the saved employee by ID
        Employee savedEmployee = employeeRepository.findById(employee.getId()).orElse(null);

        // Assert
        assertThat(savedEmployee).isNotNull();
        assertThat(savedEmployee.getId()).isEqualTo(employee.getId());
        assertThat(savedEmployee.getFirstName()).isEqualTo("John");
        assertThat(savedEmployee.getLastName()).isEqualTo("Doe");
        assertThat(savedEmployee.getDescription()).isEqualTo("Description");
        assertThat(savedEmployee.getVersion()).isEqualTo(0L);
        assertThat(savedEmployee.getManager()).isNull();
    }
}
